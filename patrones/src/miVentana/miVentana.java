/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * miVentana.java
 *
 * Created on 24-may-2013, 9:54:18
 */

package miVentana;

import curso.estrategia.AvionComercial;
import curso.estrategia.AvionRapido;
import curso.estrategia.EnAire;
import curso.estrategia.EnAireVeloz;
import curso.estrategia.EnTierra;

/**
 *
 * @author profesor
 */
public class miVentana extends javax.swing.JFrame {

    /** Creates new form miVentana */
    public miVentana() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        texto1 = new javax.swing.JLabel();
        botonComercial = new javax.swing.JButton();
        botonRapido = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        botonAire = new javax.swing.JButton();
        botonAireVeloz = new javax.swing.JButton();
        botonEnTierra = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaResultado = new javax.swing.JTextArea();
        botonMover = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        texto1.setText("Elija un avión para viajar");

        botonComercial.setText("Comercial");
        botonComercial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComercialActionPerformed(evt);
            }
        });

        botonRapido.setText("Rapido");
        botonRapido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRapidoActionPerformed(evt);
            }
        });

        jLabel2.setText("Elija la condición");

        botonAire.setText("Aire");
        botonAire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAireActionPerformed(evt);
            }
        });

        botonAireVeloz.setText("Aire veloz");
        botonAireVeloz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAireVelozActionPerformed(evt);
            }
        });

        botonEnTierra.setText("En tierra");
        botonEnTierra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEnTierraActionPerformed(evt);
            }
        });

        jLabel3.setText("El resultado es:");

        areaResultado.setColumns(20);
        areaResultado.setRows(5);
        jScrollPane1.setViewportView(areaResultado);

        botonMover.setText("Mover");
        botonMover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMoverActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonMover))
                    .addComponent(jLabel3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonAire)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonAireVeloz)
                        .addGap(12, 12, 12)
                        .addComponent(botonEnTierra))
                    .addComponent(jLabel2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(botonComercial)
                        .addGap(18, 18, 18)
                        .addComponent(botonRapido))
                    .addComponent(texto1))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(texto1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonComercial)
                    .addComponent(botonRapido))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAire)
                    .addComponent(botonAireVeloz)
                    .addComponent(botonEnTierra))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(238, Short.MAX_VALUE)
                .addComponent(botonMover)
                .addGap(64, 64, 64))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComercialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComercialActionPerformed
        // TODO add your handling code here:
        avionComercial = new AvionComercial();
        areaResultado.setText("El " + avionComercial.toString() + "...");
    }//GEN-LAST:event_botonComercialActionPerformed

    private void botonRapidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRapidoActionPerformed
        // TODO add your handling code here:
        avionRapido = new AvionRapido();
        areaResultado.setText("El " + avionRapido.toString() + "...");
    }//GEN-LAST:event_botonRapidoActionPerformed

    private void botonAireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAireActionPerformed
        // TODO add your handling code here:
        if(avionRapido==null){
         avionComercial.setAlgoritmo(new EnAire());
        }else{
            avionRapido.setAlgoritmo(new EnAire());
        }
    }//GEN-LAST:event_botonAireActionPerformed

    private void botonAireVelozActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAireVelozActionPerformed
        // TODO add your handling code here:
        if(avionRapido==null){
        avionComercial.setAlgoritmo(new EnAireVeloz());
        }else{
         avionRapido.setAlgoritmo(new EnAireVeloz());
        }
    }//GEN-LAST:event_botonAireVelozActionPerformed

    private void botonEnTierraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEnTierraActionPerformed
        // TODO add your handling code here:
         if(avionRapido==null){
        avionComercial.setAlgoritmo(new EnTierra());
        }else{
             avionRapido.setAlgoritmo(new EnTierra());
        }
    }//GEN-LAST:event_botonEnTierraActionPerformed

    private void botonMoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMoverActionPerformed
        // TODO add your handling code here:
         if(avionRapido==null){
         areaResultado.setText(avionComercial.mover());
        }else{
              areaResultado.setText(avionRapido.mover());
        }
    }//GEN-LAST:event_botonMoverActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new miVentana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaResultado;
    private javax.swing.JButton botonAire;
    private javax.swing.JButton botonAireVeloz;
    private javax.swing.JButton botonComercial;
    private javax.swing.JButton botonEnTierra;
    private javax.swing.JButton botonMover;
    private javax.swing.JButton botonRapido;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel texto1;
    // End of variables declaration//GEN-END:variables

    private static AvionComercial avionComercial=null;
    private static AvionRapido avionRapido=null;
}
